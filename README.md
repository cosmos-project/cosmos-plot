# COSMOS Plot Tool #

The COSMOS Plot Tool can plot any data available in the COSMOS network. 

Example:
 - readings from the ADCS
 - battery level from the EPS

# Configuration File #
The config file is a JSON file which follows a simple JSON pattern in order for COSMOS plot to keep track of the data and format you want plotted.
The config file that is currently being used is called "plots.json" which looks at both EPS and ADCS data.
 
# Install / Update Steps #
1. Clone cosmos/core and make sure it is updated
    - to update cosmos/core:
     - pull latest changes from gitkraken or sourcetree
     - open the cosmos/core project in Qt Creator
     - in the project settings of Qt Creator, under the build tab, look for 'build steps' and make sure that you have selected 'install' as your build target
     - build the project
2. Clone/Update repository to ~/cosmos/source/tools/cosmos-plot
3. Open the ~/cosmos/source/tools/cosmos-plot/CosmosPlot.pro file
4. Build the project
5. Run the project

- if you have errors with opengl libraries
	$ apt-get install freeglut3 freeglut3-dev libgl1-mesa-dev